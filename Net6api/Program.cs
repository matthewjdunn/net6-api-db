using Microsoft.EntityFrameworkCore;
using Net6api.Middleware;
using Net6api.Models;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddDbContext<APIContext>(options =>
{
    options.UseSqlite(builder.Configuration.GetConnectionString("APIdb"));
});


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ResponseTime>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }  // required for tests, otherwise deps.json error