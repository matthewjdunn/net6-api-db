﻿using Microsoft.EntityFrameworkCore;
using static Net6api.Models.APIModels;

namespace Net6api.Models

{
    public class APIContext: DbContext
    {
        public APIContext(DbContextOptions options) : base(options)
        {
        }        
        public DbSet<User> Users { get; set; } = default!;

        public DbSet<Story> Stories { get; set; } = default!;

    }


}
