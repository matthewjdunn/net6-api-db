﻿using static Net6api.Models.APIModels;

namespace Net6api.Models
{
    public class DataHelper
    {
        public static void SeedData(APIContext _dbContext)
        {
            // Create the database schema
            _dbContext.Database.EnsureCreatedAsync();


            if (!_dbContext.Users.Any())
            {
                //_dbContext.Users.AddRange(new User(1, "dave@gmail.com", "Dave", "Brock"),
                //                        new User(2, "carla@gmail.com", "Carla", "Waters"),
                //                        new User(3, "emma@gmail.com", "Emma", "Johnston"),
                //                        new User(4, "jenny@gmail.com", "Jenny", "Jennifer"),
                //                        new User(5, "tony@gmail.com", "Tony", "Stark")
                //);
                _dbContext.Users.Add(new User { userId = 1, email = "dave@gmail.com", firstName = "Dave", lastName = "Brock" });
                _dbContext.Users.Add(new User { userId = 2, email = "carla@gmail.com", firstName = "Carla", lastName = "Waters" });
                _dbContext.Users.Add(new User { userId = 3, email = "emma@gmail.com", firstName = "Emma", lastName = "Johnston" });
                _dbContext.Users.Add(new User { userId = 4, email = "jenny@gmail.com", firstName = "Jenny", lastName = "Block" });
                _dbContext.Users.Add(new User { userId = 5, email = "tony@gmail.com", firstName = "Tony", lastName = "Tiger" });
                _dbContext.Users.Add(new User { userId = 6, email = "bobby@gmail.com", firstName = "Bob", lastName = "Peru" });

                _dbContext.Stories.Add(new Story { storyId = 1, title = "Upgrade to .NET 6", userId = 1, points = 3, dueDate = DateTime.Now.AddDays(9) });
                _dbContext.Stories.Add(new Story { storyId = 2, title = "Upgrade EFCore version", userId = 1, points = 5, dueDate = DateTime.Now.AddDays(7) });
                _dbContext.Stories.Add(new Story { storyId = 3, title = "Create XUnit Tests", userId = 2, points = 3, dueDate = DateTime.Now.AddDays(14) });
                _dbContext.Stories.Add(new Story { storyId = 4, title = "Load Testing", userId = 3, points = 5, dueDate = DateTime.Now.AddDays(11) });
                _dbContext.Stories.Add(new Story { storyId = 5, title = "Web API Controller", userId = 4, points = 8, dueDate = DateTime.Now.AddDays(10) });
                _dbContext.Stories.Add(new Story { storyId = 6, title = "Training", userId = 3, points = 5, dueDate = DateTime.Now.AddDays(3) });
                _dbContext.Stories.Add(new Story { storyId = 7, title = "Pair programming", userId = 4, points = 8, dueDate = DateTime.Now.AddDays(15) });
                _dbContext.Stories.Add(new Story { storyId = 8, title = "Vue JS work", userId = 2, points = 8, dueDate = DateTime.Now.AddDays(10) });
                _dbContext.Stories.Add(new Story { storyId = 9, title = "Middleware work", userId = 3, points = 2, dueDate = DateTime.Now.AddDays(30) });
                _dbContext.Stories.Add(new Story { storyId = 10, title = "JWT module", userId = 5, points = 8, dueDate = DateTime.Now.AddDays(8) });

                _dbContext.SaveChanges();
            }
        }


    }
}
