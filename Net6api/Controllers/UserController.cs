﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Net6api.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Net6api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly APIContext _dbContext;
        public UserController(APIContext dbContext)
        {
            _dbContext = dbContext;
            
            DataHelper.SeedData(_dbContext);           

        }
        // GET: api/<UserController>        
        [HttpGet("GetUsers")]
        public async Task<IActionResult> GetAsync()
        {              
            var users = await _dbContext.Users.ToListAsync();
            return Ok(users);
        }        
                      
    }
}
