using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Net6api.Controllers;
using Net6api.Models;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestAPI
{


    public class APITest
    {
        private readonly APIContext _dbContext;
        private UserController _userController;
        public APITest()
        {

            _dbContext = GetAPIContext();          
            
            DataHelper.SeedData(_dbContext);

        }
        
        [Fact]
        public async Task GetUsersAsync_Success_Test()
        {

            // Act
            var users = await _dbContext.Users.ToListAsync();
            
            // Assert
            Assert.NotEmpty(users);
        }

        [Fact]
        public async Task GetStoriesAsync_Success_Test()
        {

            // Act
            var stories = await _dbContext.Stories.ToListAsync();

            // Assert
            Assert.NotEmpty(stories);
        }       

        [Fact]
        public async Task Should_Return_All_Users()
        {
            //Arrange            
            _userController = new UserController(_dbContext);
            //Act
            var result = await _userController.GetAsync();


            //Assert
            Assert.IsType<OkObjectResult>(result);




        }
        private APIContext GetAPIContext()
        {
            var optBuilder = new DbContextOptionsBuilder<APIContext>()
                             .UseSqlite("DataSource = file::memory:?cache = shared");
            var dbContext = new APIContext(optBuilder.Options);
            
            dbContext.Database.EnsureCreated();
            
            return dbContext;
        }
    }
}