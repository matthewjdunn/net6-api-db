﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestAPI
{
    public class WeatherControllerTests : IClassFixture<ApiWebApplicationFactory>
    {
        readonly HttpClient _client;

        public WeatherControllerTests(ApiWebApplicationFactory application)
        {
            _client = application.CreateClient();
        }
        [Fact]
        public async Task GET_retrieves_weather_forecast()
        {
            var response = await _client.GetAsync("/weatherforecast");
            Assert.Equal(HttpStatusCode.OK, (HttpStatusCode)response.StatusCode);           

        }
    }

    public class ApiWebApplicationFactory : WebApplicationFactory<Program>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration(config => { });
            builder.ConfigureTestServices(services => { });
        }
    }
}
